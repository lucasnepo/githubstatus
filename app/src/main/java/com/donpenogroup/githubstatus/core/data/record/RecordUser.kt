package com.donpenogroup.githubstatus.core.data.record

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.donpenogroup.githubstatus.core.data.ParcelableControll

object TABLE_USER{
    const val TABLE_NAME = "user_table"
    const val COLUMN_UUID = "uuid_column"
    const val COLUMN_AVATAR = "avatar_column"
    const val COLUMN_BIO = "bio_column"
    const val COLUMN_EMAIL = "email_column"
    const val COLUMN_FOLLOWERS = "followers_column"
    const val COLUMN_FOLLOWERS_URL = "followers_url_column"
    const val COLUMN_FOLLOWING = "following_column"
    const val COLUMN_FOLLOWING_URL = "following_url_url"
    const val COLUMN_HTML_URL = "html_url_column"
    const val COLUMN_GITHUB_ID = "github_id_column"
    const val COLUMN_LOCATION = "location_column"
    const val COLUMN_USER_NAME = "user_name_column"
    const val COLUMN_PUBLIC_REPO = "public_repo_column"
    const val COLUMN_REPOS_URL = "repos_url_column"
}

@Entity(tableName = TABLE_USER.TABLE_NAME)
data class RecordUser(

    @PrimaryKey
    @ColumnInfo(name = TABLE_USER.COLUMN_UUID)
    var uuid: String? = "",

    @ColumnInfo(name = TABLE_USER.COLUMN_AVATAR)
    val avatar_url: String? = "",

    @ColumnInfo(name = TABLE_USER.COLUMN_BIO)
    val bio: String? = "",

    @ColumnInfo(name = TABLE_USER.COLUMN_EMAIL)
    val email: String? = "",

    @ColumnInfo(name = TABLE_USER.COLUMN_FOLLOWERS)
    val followers: Int = 0,

    @ColumnInfo(name = TABLE_USER.COLUMN_FOLLOWERS_URL)
    val followers_url: String? = "",

    @ColumnInfo(name = TABLE_USER.COLUMN_FOLLOWING)
    val following: Int = 0,

    @ColumnInfo(name = TABLE_USER.COLUMN_FOLLOWING_URL)
    val following_url: String? = "",

    @ColumnInfo(name = TABLE_USER.COLUMN_HTML_URL)
    val html_url: String? = "",

    @ColumnInfo(name = TABLE_USER.COLUMN_GITHUB_ID)
    val id: Int = 0,

    @ColumnInfo(name = TABLE_USER.COLUMN_LOCATION)
    val location: String? = "",

    @ColumnInfo(name = TABLE_USER.COLUMN_USER_NAME)
    val name: String? = "",

    @ColumnInfo(name = TABLE_USER.COLUMN_PUBLIC_REPO)
    val public_repos: Int = 0,

    @ColumnInfo(name = TABLE_USER.COLUMN_REPOS_URL)
    val repos_url: String? = ""

) : Parcelable{

    constructor(parcel: Parcel) : this(
        uuid = parcel.readString(),
        avatar_url = parcel.readString(),
        bio = parcel.readString(),
        email = parcel.readString(),
        followers = parcel.readInt(),
        followers_url = parcel.readString(),
        following = parcel.readInt(),
        following_url = parcel.readString(),
        html_url = parcel.readString(),
        id = parcel.readInt(),
        location = parcel.readString(),
        name = parcel.readString(),
        public_repos = parcel.readInt(),
        repos_url = parcel.readString()
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(uuid)
        dest?.writeString(avatar_url)
        dest?.writeString(bio)
        dest?.writeString(email)
        dest?.writeInt(followers)
        dest?.writeString(followers_url)
        dest?.writeInt(following)
        dest?.writeString(following_url)
        dest?.writeString(html_url)
        dest?.writeInt(id)
        dest?.writeString(location)
        dest?.writeString(name)
        dest?.writeInt(public_repos)
        dest?.writeString(repos_url)
    }

    override fun describeContents(): Int = ParcelableControll().POJO_RECORD_USER

    companion object CREATOR : Parcelable.Creator<RecordUser> {
        override fun createFromParcel(parcel: Parcel): RecordUser {
            return RecordUser(parcel)
        }

        override fun newArray(size: Int): Array<RecordUser?> {
            return arrayOfNulls(size)
        }
    }
}