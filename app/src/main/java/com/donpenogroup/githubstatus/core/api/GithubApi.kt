package com.donpenogroup.githubstatus.core.api

import com.donpenogroup.githubstatus.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object GithubApi {

    fun getInstance(): GithubService = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_API_GITHUB)
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(GithubService::class.java)


}