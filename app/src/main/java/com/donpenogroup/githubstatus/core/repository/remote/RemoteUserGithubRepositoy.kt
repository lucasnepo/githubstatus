package com.donpenogroup.githubstatus.core.repository.remote

import android.util.Log
import com.donpenogroup.githubstatus.core.api.GithubApi
import com.donpenogroup.githubstatus.core.data.payload.PayloadUser
import com.donpenogroup.githubstatus.core.repository.RemoteRepository

class RemoteUserGithubRepositoy : RemoteRepository<PayloadUser, String>{

    private val githubApi = GithubApi.getInstance()

    override suspend fun all(): List<PayloadUser> {
        val list_ : List<PayloadUser> = ArrayList()
        return list_
    }

    override suspend fun single(search: String): PayloadUser? {
        try {
            val response = githubApi.getUser(search)

            if(!response.isSuccessful){
                Log.e("REMOTE", "Server response unsuccessful. Code $response.code() ")
                return null
            }

            if (response.body() != null) return response.body()

        }catch (e: Exception){
            return null
        }
        return null
    }

}