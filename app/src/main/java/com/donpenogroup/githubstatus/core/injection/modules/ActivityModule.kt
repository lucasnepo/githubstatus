package com.donpenogroup.githubstatus.core.injection.modules

import com.donpenogroup.githubstatus.core.annotations.ActivityScoped
import com.donpenogroup.githubstatus.search.activitys.SearchActivity
import com.donpenogroup.githubstatus.search.di.SearchModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [SearchModule::class])
    internal abstract fun searchActivity() : SearchActivity

}