package com.donpenogroup.githubstatus.core.databinding

import android.graphics.drawable.Drawable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

class BindButton(
    private val _name : String? = "",
    private val _image : Drawable? = null
) : BaseObservable(){

    @get:Bindable
    var name : String? = _name
    set(value) {
       field = value
       notifyPropertyChanged(BR.name)
    }

    @get:Bindable
    var image : Drawable? = _image
    set(value){
        field = value
        notifyPropertyChanged(BR.image)
    }

}