package com.donpenogroup.githubstatus.core.extensions

fun String.verifyIsNullAndBlank():Boolean
        = this != null && !this.isBlank()

