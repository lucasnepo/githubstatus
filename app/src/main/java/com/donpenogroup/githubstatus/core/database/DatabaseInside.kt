package com.donpenogroup.githubstatus.core.database

import androidx.room.RoomDatabase

interface DatabaseInside<T, C> {
    fun getInstance(context : C) : T?
    fun onDestroy()
}