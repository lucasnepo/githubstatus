package com.donpenogroup.githubstatus.core.api

import com.donpenogroup.githubstatus.BuildConfig
import com.donpenogroup.githubstatus.core.data.payload.PayloadUser
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubService {

    @GET(BuildConfig.API_GITHUB_DOWNLOAD_USER)
    suspend fun getUser(
        @Path(BuildConfig.PATH_GITHUB_USER) user: String)
            : Response<PayloadUser>

}