package com.donpenogroup.githubstatus.core.injection.modules

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

@Module
abstract class GithubStatusModule{

    @Binds
    abstract fun bindContext(applicationContext: Application) : Context

}