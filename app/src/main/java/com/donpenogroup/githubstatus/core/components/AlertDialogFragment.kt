package com.donpenogroup.githubstatus.core.components

import android.os.Bundle
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.donpenogroup.githubstatus.R
import com.donpenogroup.githubstatus.core.injection.remade.DaggerBottomSheet
import com.donpenogroup.githubstatus.databinding.FragmentAlertErrorBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_alert_error.*

class AlertDialogFragment : BottomSheetDialogFragment{

    private lateinit var binding: FragmentAlertErrorBinding
    private var _message : String = ""

    constructor(message: String) : super(){
        _message = message
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_alert_error, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClickListener()
    }

    private fun onClickListener() {
        alert_message.text = _message

        button_exit.setOnClickListener {
            dismiss()
        }
    }
}