package com.donpenogroup.githubstatus.core

import com.donpenogroup.githubstatus.core.injection.DaggerInjectionGithubStatus
import com.donpenogroup.githubstatus.core.injection.InjectionGithubStatus
import dagger.android.DaggerApplication

class GithubStatusApplication : DaggerApplication(){

    override fun applicationInjector(): InjectionGithubStatus {
        return DaggerInjectionGithubStatus
                .builder()
                .application(this)
                .build()
    }


}