package com.donpenogroup.githubstatus.core.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.donpenogroup.githubstatus.BuildConfig
import com.donpenogroup.githubstatus.core.data.record.RecordUser

@Database(
    entities = [RecordUser::class],
    version = BuildConfig.VERSION_CODE,
    exportSchema = BuildConfig.EXPORT_SCHEMA)
abstract class DatabaseGithubStatus : RoomDatabase(){

    companion object Singleton{
        var INSTANCE : DatabaseGithubStatus? = null

        fun getDatabase(ctx:Context):DatabaseGithubStatus{
            if (INSTANCE == null){
                INSTANCE = Room.databaseBuilder(
                    ctx.applicationContext,
                    DatabaseGithubStatus::class.java,
                    BuildConfig.DATABASE_NAME).build()
            }
            return INSTANCE!!
        }
    }

}