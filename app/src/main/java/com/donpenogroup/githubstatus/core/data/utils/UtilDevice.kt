package com.donpenogroup.githubstatus.core.data.utils

data class UtilDevice(
    var width : Int = 0,
    var height: Int = 0
)