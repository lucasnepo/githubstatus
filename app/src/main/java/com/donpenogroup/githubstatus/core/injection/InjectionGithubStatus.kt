package com.donpenogroup.githubstatus.core.injection

import android.app.Application
import com.donpenogroup.githubstatus.core.annotations.ApplicationScoped
import com.donpenogroup.githubstatus.core.injection.modules.ActivityModule
import com.donpenogroup.githubstatus.core.injection.modules.FragmentModule
import com.donpenogroup.githubstatus.core.injection.modules.GithubStatusModule
import dagger.*
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule

@ApplicationScoped
@Component(modules = [
    GithubStatusModule::class,
    ActivityModule::class,
    FragmentModule::class,
    AndroidSupportInjectionModule::class
])
interface InjectionGithubStatus : AndroidInjector<DaggerApplication>{

    @Component.Builder
    interface Builder{

        @BindsInstance
        fun application(application: Application) : Builder

        fun build() : InjectionGithubStatus
    }


}