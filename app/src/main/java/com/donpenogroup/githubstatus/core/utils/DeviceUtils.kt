package com.donpenogroup.githubstatus.core.utils

import android.util.DisplayMetrics
import android.view.WindowManager
import com.donpenogroup.githubstatus.core.data.utils.UtilDevice

class DeviceUtils {

    private val displayMetrics = DisplayMetrics()

    fun getDimensionsDevice(window : WindowManager) : UtilDevice{

        var device = UtilDevice()
        window.defaultDisplay.getMetrics(displayMetrics)

        device.apply {
            width = displayMetrics.widthPixels
            height = displayMetrics.heightPixels
        }

        return device
    }

}