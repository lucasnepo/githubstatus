package com.donpenogroup.githubstatus.core.repository

/*
    T = Tipo de retorno
    S = Tipo do parâmetro de busca
*/
interface RemoteRepository <T, S>{
    suspend fun all() : List<T>
    suspend fun single(search : S): T?
}