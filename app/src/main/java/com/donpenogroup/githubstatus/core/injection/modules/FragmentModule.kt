package com.donpenogroup.githubstatus.core.injection.modules

import com.donpenogroup.githubstatus.core.annotations.FragmentScoped
import com.donpenogroup.githubstatus.core.components.AlertDialogFragment
import com.donpenogroup.githubstatus.search.di.FragmentSettingModule
import com.donpenogroup.githubstatus.search.fragments.SettingBottomSheetDialogFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @FragmentScoped
    @ContributesAndroidInjector(modules = [FragmentSettingModule::class])
    internal abstract fun settingFragment(): SettingBottomSheetDialogFragment



}