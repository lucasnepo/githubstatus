package com.donpenogroup.githubstatus.search.mvvm.model

import com.donpenogroup.githubstatus.core.repository.remote.RemoteUserGithubRepositoy
import javax.inject.Inject

class SearchModel @Inject constructor(private val remoteUserRepository:RemoteUserGithubRepositoy) {

    suspend fun searchUserNetwork(search:String) = remoteUserRepository.single(search)


}