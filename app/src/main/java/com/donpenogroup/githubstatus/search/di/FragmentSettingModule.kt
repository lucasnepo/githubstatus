package com.donpenogroup.githubstatus.search.di

import dagger.Module
import dagger.Provides

@Module
class FragmentSettingModule {

    @Provides
    internal fun provideDaggerStartd(): String {
        return "Dagger iniciado com sucesso no fragmento"
    }

}