package com.donpenogroup.githubstatus.search.di

import com.donpenogroup.githubstatus.core.annotations.ActivityScoped
import com.donpenogroup.githubstatus.core.repository.remote.RemoteUserGithubRepositoy
import com.donpenogroup.githubstatus.search.mvvm.model.SearchModel
import dagger.Module
import dagger.Provides

@Module
class SearchModule {

    @ActivityScoped
    @Provides
    fun providerModel() : SearchModel = SearchModel(RemoteUserGithubRepositoy())

}