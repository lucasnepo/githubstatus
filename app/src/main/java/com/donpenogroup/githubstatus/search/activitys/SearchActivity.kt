package com.donpenogroup.githubstatus.search.activitys

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.donpenogroup.githubstatus.R
import com.donpenogroup.githubstatus.core.components.AlertDialogFragment
import com.donpenogroup.githubstatus.core.data.record.RecordUser
import com.donpenogroup.githubstatus.databinding.SearchMainActivityBinding
import com.donpenogroup.githubstatus.search.SearchSharedKEY
import com.donpenogroup.githubstatus.search.fragments.SettingBottomSheetDialogFragment
import com.donpenogroup.githubstatus.search.mvvm.model.SearchModel
import com.donpenogroup.githubstatus.search.mvvm.viewmodel.SearchViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.search_main_activity.*
import javax.inject.Inject

class SearchActivity : DaggerAppCompatActivity() , SearchSharedKEY {

    private lateinit var binding   : SearchMainActivityBinding
    private lateinit var viewModel : SearchViewModel

    @Inject lateinit var model   : SearchModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
    }

      private fun setupView() {
        viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        viewModel.attachSearchModel(model)

        viewModel.loading.observe(this,showLoading())
        viewModel.userfound.observe(this,detailsUsers())
        viewModel.message.observe(this,alertMessage())

        binding = DataBindingUtil.setContentView(this, R.layout.search_main_activity)

        binding.avatar.startAnimation(AnimationUtils.loadAnimation(this,
            R.anim.anim_start
        ))

        binding.searchUser.setOnClickListener (searchUserNow())
        setSupportActionBar(binding.bottomAppBar)
    }

    private fun alertMessage() = Observer<String> {
        val alert = AlertDialogFragment(it)
        alert.show(supportFragmentManager,alert.tag)
        textInputLayout.editText?.setText("")
    }

    private fun detailsUsers() = Observer<RecordUser> {
        var intentDetails = Intent(this,ProfileActivity::class.java)
        intentDetails.putExtra(USERFOUND,it)
        startActivity(intentDetails)
    }

    private fun showLoading()= Observer<Boolean> {
        when (it){
           true-> binding.loading.visibility = View.VISIBLE
           else->  binding.loading.visibility = View.GONE
        }
    }

    private fun searchUserNow() = View.OnClickListener {
        viewModel.searchUser(binding.textInputLayout.editText?.text.toString())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_item_settings ->{
               val settingFragment = SettingBottomSheetDialogFragment()
                   settingFragment.show(supportFragmentManager,settingFragment.tag)
            }

        }
        return true
    }

}
