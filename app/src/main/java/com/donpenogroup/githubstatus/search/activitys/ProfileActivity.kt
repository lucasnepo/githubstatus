package com.donpenogroup.githubstatus.search.activitys

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.donpenogroup.githubstatus.R
import com.donpenogroup.githubstatus.core.data.record.RecordUser
import com.donpenogroup.githubstatus.core.listenersUtils.OnTouchScroll
import com.donpenogroup.githubstatus.databinding.ProfileDetailsActivityBinding
import com.donpenogroup.githubstatus.search.SearchSharedKEY
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.profile_details_activity.*

class ProfileActivity : AppCompatActivity(), SearchSharedKEY {

    private lateinit var binding: ProfileDetailsActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.profile_details_activity)

        val userfound = intent.extras?.getParcelable<RecordUser>(USERFOUND)
        actionbarConfig(userfound?.name)
        binding.user = userfound
        Picasso.with(this).load(userfound?.avatar_url).into(profile_image)
    }

    private fun actionbarConfig(name: String?) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.elevation = 0.0f
        supportActionBar?.title = name
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}