package com.donpenogroup.githubstatus.search.mvvm.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class ProfileViewModel(application: Application) : AndroidViewModel(application) {


    private val _loading = MutableLiveData<Boolean>()

    fun progressImageView(link: String){
        _loading.postValue(true)

    }

}