package com.donpenogroup.githubstatus.search.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.donpenogroup.githubstatus.R
import com.donpenogroup.githubstatus.core.injection.remade.DaggerBottomSheet
import com.donpenogroup.githubstatus.databinding.SettingFragmentBinding
import javax.inject.Inject

class SettingBottomSheetDialogFragment : DaggerBottomSheet() {

    private lateinit var binding: SettingFragmentBinding

    @Inject
    lateinit var message : String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.setting_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClickListener()
    }

    private fun onClickListener(){
        binding.navigationView.setNavigationItemSelectedListener {menuItem ->
            when (menuItem!!.itemId) {
                R.id.menu_item_permissions -> {
                    toastInFragment(message)
                }
                R.id.menu_item_users -> {
                    toastInFragment("Aplicativo em desenvolvimento")
                }
                R.id.menu_item_project ->{
                    toastInFragment("https://bitbucket.org/lucasnepo/githubstatus/src/master/")
                }
            }
            true
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    private fun toastInFragment(message:String){
        Toast.makeText(binding.root.context,message,Toast.LENGTH_SHORT).show()
    }

}