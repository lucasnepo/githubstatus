package com.donpenogroup.githubstatus.search.mvvm.viewmodel

import androidx.lifecycle.*
import com.donpenogroup.githubstatus.core.data.record.RecordUser
import com.donpenogroup.githubstatus.core.extensions.verifyIsNullAndBlank
import com.donpenogroup.githubstatus.search.mvvm.model.SearchModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchViewModel : ViewModel() {
    private var model: SearchModel? = null

    private val _loading  = MutableLiveData<Boolean>()
    private val _message  = MutableLiveData<String>()
    private val _userfound = MutableLiveData<RecordUser>()

    fun getTestViewModel():String{
        return "ViewModel injetado com sucesso"
    }

    fun searchUser(user:String){
        _loading.value = true
        when (user.verifyIsNullAndBlank()){
            true -> hasInformation(user)
            false-> onInvalidValueInField()
        }
    }

    fun attachSearchModel(model: SearchModel){
        this.model = model
    }

    private fun hasInformation(user:String) {
        viewModelScope.launch(Dispatchers.IO){
            val userFound = model?.searchUserNetwork(user)
            if (userFound != null)
                _userfound.postValue(userFound.toRecordUser())
            else  _message.postValue("Usuário não encontrado...")

            _loading.postValue(false)
        }
    }

    private fun onInvalidValueInField(){
        _loading.value = false
        _message.value = "Field not fill, sorry"
    }

    val message =  _message
    val loading = _loading
    val userfound = _userfound
}